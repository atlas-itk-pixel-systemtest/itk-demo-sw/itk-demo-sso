from itk_demo_sso.sqlalchemy_models import Base
from itk_demo_sso.tools import compressToBytes, decompressBytes
# from itk_demo_sso.base_payload import BasePayload
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship
import json
from sqlalchemy_utils import UUIDType
from uuid import uuid4
from sqlalchemy.orm import declarative_base

class UserInfo(Base):
    """
    ORM-class that represents the userinfo-table
    """ 

    __tablename__ = "user_info"
    id = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_name = Column(String(255), nullable=False)
    timestamp = Column(Integer, nullable=False)
    url_accessed = Column(String, nullable=False)
    cern_roles = Column(String(255), nullable=False)
    type = Column(String(255), nullable=False)

    # tags = relationship("Tag", secondary=tag_payload, back_populates="payloads")
    # objects = relationship("Object", secondary=object_payload, back_populates="payloads")

    def __init__(self, type, user_name=None, timestamp=None, url_accessed=None, cern_roles=None, id=None):
        if id is not None:
            self.id = id
        if user_name is not None:
            self.user_name = user_name
        if timestamp is not None:
            self.timestamp = timestamp
        if url_accessed is not None:
            self.url_accessed = url_accessed
        if cern_roles is not None:
            self.cern_roles = cern_roles
        self.type = type

    # def to_dict(self, payload_data=True, decode=True, format=False):
    #     rep = {"id": self.id.hex, "type": self.type, "name": self.name, "meta": False}
    #     if payload_data:
    #         if self.data is not None:
    #             rep["data"] = self.get_data(decode=decode, format=format)
    #     return rep

    # def get_data(self, decode=True, format=False):
    #     if decode:
    #         if format:
    #             data = decompressBytes(self.data)
    #             try:
    #                 data = json.loads(data)
    #                 data = json.dumps(data, indent=4)
    #             except json.JSONDecodeError:
    #                 pass
    #             return data
    #         else:
    #             return decompressBytes(self.data)
    #     else:
    #         return self.data

    # def set_data(self, data, encode=True):
    #     if encode:
    #         self.data = compressToBytes(data)
    #     else:
    #         self.data = data

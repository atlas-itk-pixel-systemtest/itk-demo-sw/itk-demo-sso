from connexion import FlaskApp
from connexion.exceptions import ProblemException
from flask_cors import CORS
from itk_demo_sso.data_base import SQLAlchemyBaseAdapter  # Import SQLAlchemyBaseAdapter

# from rcf_response import Error
from flask import redirect, request, redirect, url_for, session
import itk_demo_sso.sso as sso
from flask_session import Session


def create_app(config=None):
    app = FlaskApp(
        __name__,
        specification_dir="./openapi/",
        options={
            "swagger_ui": True,
            "swagger_url": "/docs",
        },
        server_args={},
    )

    flask_app = app.app
    CORS(flask_app)
    flask_app.secret_key = "duihduiwdhiwufbuw3e"
    flask_app.register_blueprint(sso.sso_bp, url_prefix="/sso")
    # flask_app.config["SESSION_TYPE"] = "filesystem"  # You can also use "redis", "memcached", etc.
    # flask_app.config["SESSION_PERMANENT"] = False
    # flask_app.config["SESSION_USE_SIGNER"] = True
    # flask_app.config["SESSION_KEY_PREFIX"] = "session:"
    # flask_app.config["SESSION_FILE_DIR"] = "/tmp/flask_session"  # Directory to store session files
    #     # flask_app.register_blueprint(sso.sso_bp)
    # Session(flask_app)

    # Initialize SQLAlchemyBaseAdapter
    db_adapter = SQLAlchemyBaseAdapter(owner_url='sqlite:///logs.db')
    db_adapter.create_database()
    db_adapter.initialize_tables()
    def log_error(name, level, message):
        session = db_adapter.create_session()
        db_adapter.create_payload(session, type='error', data={'name': name, 'level': level, 'message': message})


    with flask_app.app_context():
        sso.oauth = sso.create_oauth(flask_app)
        app.add_api("openapi.yml")

    @flask_app.errorhandler(404)
    def handle_404(error):
        return flask_error_response(error)

    @flask_app.errorhandler(405)
    def handle_405(error):
        return flask_error_response(error)

    @flask_app.errorhandler(500)
    def handle_500(error):
        return flask_error_response(error)

    @flask_app.errorhandler(ProblemException)
    def handle_exception(error):
        return connexion_error_response(error)

    return flask_app


def flask_error_response(error):
    error_response = {
        "code": error.code,
        "name": error.name,
        "description": f"{error.description} ({request.url}).",
    }
    return error_response, error.code


def connexion_error_response(error):
    error_response = {
        "status": error.status,
        "title": error.title,
        "detail": error.detail,
    }
    return error_response, error.status


# adapted from http://flask.pocoo.org/snippets/35/
class ReverseProxied:
    """Wrap the application in this middleware and configure the
    reverse proxy to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:

    location /proxied/ {
        proxy_pass http://192.168.0.1:5001/;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Forwarded-Path /proxied;
    }

    :param app: the WSGI application
    :param script_name: override the default script name (path)
    :param scheme: override the default scheme
    :param server: override the default server
    """

    def __init__(self, app, script_name=None, scheme=None, server=None):
        self.app = app
        self.script_name = script_name
        self.scheme = scheme
        self.server = server

    def __call__(self, environ, start_response):
        script_name = environ.get("HTTP_X_FORWARDED_PATH", "") or self.script_name
        if script_name:
            environ["SCRIPT_NAME"] = "/" + script_name.lstrip("/")
            path_info = environ["PATH_INFO"]
            if path_info.startswith(script_name):
                environ["PATH_INFO_OLD"] = path_info
                environ["PATH_INFO"] = path_info[len(script_name) :]
        scheme = environ.get("HTTP_X_SCHEME", "") or self.scheme
        if scheme:
            environ["wsgi.url_scheme"] = scheme
        server = environ.get("HTTP_X_FORWARDED_SERVER", "") or self.server
        if server:
            environ["HTTP_HOST"] = server
        return self.app(environ, start_response)


# if __name__ == "__main__":
#     app = create_app()
#     host = "wupp-charon.cern.ch"
#     port = 5002
#     app.run(host=host, port=port)

# db = SQLAlchemy(app)

# class LogEntry(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     level = db.Column(db.String(50))
#     message = db.Column(db.String(500))
#     timestamp = db.Column(db.DateTime, default=datetime.utcnow)

#     def __repr__(self):
#         return f'<Log {self.level}: {self.message}>'
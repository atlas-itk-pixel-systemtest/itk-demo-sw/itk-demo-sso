from itk_demo_sso.model import PayloadModel
from sqlalchemy import Column, String
from sqlalchemy_utils import UUIDType
from uuid import uuid4
from abc import ABC, abstractmethod

class BasePayload(PayloadModel, ABC):
    """
    ORM-class that represents the payload-table
    """

    __tablename__ = "payload"

    id = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    type = Column(String(255), nullable=False)
    name = Column(String(255))

    def to_tree(self, payload_data=0, depth=-1):
        return self.to_dict(payload_data=payload_data)

    @abstractmethod
    def get_data(self):
        pass

    @abstractmethod
    def set_data(self):
        pass

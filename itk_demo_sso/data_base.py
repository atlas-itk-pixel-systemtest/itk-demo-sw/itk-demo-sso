# from configdb_server.models.sqlalchemy.tag import Tag
from itk_demo_sso.profiling import SQLAlchemyProfiling
from itk_demo_sso.UserInfo import UserInfo
from sqlalchemy_utils import (
    database_exists as database_exists_sqla,
    create_database as create_database_sqla,
)
from sqlalchemy.exc import (
    OperationalError,
    IntegrityError,
)
from itk_demo_sso.exceptions import (
    DatasetNotFoundError,
    IDInUseError,
)
from sqlalchemy import create_engine, desc, event
from sqlalchemy.orm import sessionmaker, Session

from sqlalchemy.orm import declarative_base
from itk_demo_sso.sqlalchemy_models import Base
import json

class SQLAlchemyBaseAdapter():
    def __init__(self, owner_url):
        self.owner_url = owner_url
        self.owner_engine = create_engine(self.owner_url, pool_pre_ping=True)

        self.profile = SQLAlchemyProfiling(self.owner_engine)

    def __del__(self):
        self.owner_engine.dispose()

    def create_session(self):
        write_session = sessionmaker(bind=self.owner_engine, future=True)()
        return write_session


    def create_log_entry(self, write_session, userinfo) -> str:
        # Parse the JSON object
        # Extract data
        user_name = userinfo["user_name"]
        timestamp = userinfo["timestamp"]
        url_accessed = userinfo["url_accessed"]
        cern_roles = userinfo["cern_roles"]
        type = userinfo["type"]
        # Create an instance of UserInfo
        user_info = UserInfo(user_name=user_name, timestamp=timestamp, url_accessed=url_accessed, cern_roles=cern_roles, type=type)
        
        try:
            write_session.add(user_info)
            write_session.commit()
        except IntegrityError:
            write_session.rollback()
            raise IDInUseError
        return user_info.id.hex

    def query_logs(self, session: Session):
        dataset = session.query(UserInfo)
        if dataset is None:
            raise DatasetNotFoundError
        return dataset


    # Database functions
    def __database_exists(self):
        try:
            return database_exists_sqla(self.owner_url)
        except TypeError:
            pass

    def database_running(self) -> bool:
        try:
            # Create an engine
            engine = create_engine(self.owner_url)

            # Try to establish a connection
            with engine.connect() as connection:
                pass
        except OperationalError:
            # If an OperationalError is raised, the connection could not be established
            return False
        return True

    def database_exists(self) -> bool:
        if database_exists_sqla(self.owner_url):
            return True
        return False

    def create_database(self):
        try:
            if not self.database_exists():
                create_database_sqla(self.owner_url)
        except (AttributeError, TypeError, OperationalError):
            pass
    def initialize_tables(self):
        """
        Initialize the tables in the database using metadata.
        """
        Base.metadata.create_all(self.owner_engine)

        # self.__init__(self.owner_url, self.read_url, self.write_url)

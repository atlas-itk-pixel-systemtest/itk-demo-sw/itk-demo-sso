"""Flask Blueprint for SSO authentication.

Manages user authentication with CERN SSO. This is an OIDC application. If the authentication process is successful,
sets the userinfo in the session allowing persistence between requests.
"""
from itk_demo_sso.data_base import SQLAlchemyBaseAdapter
from flask import Blueprint
from flask import (
    url_for,
    session,
    jsonify,
    request,
    redirect,
    current_app,
    make_response,
)
from authlib.integrations.flask_client import OAuth, OAuthError

import time
from jwt import PyJWKClient
from urllib.parse import urlencode

# Initialize SQLAlchemyBaseAdapter
db_adapter = SQLAlchemyBaseAdapter(owner_url='sqlite:///logs.db')

def log_user_info(userinfo):
    session = db_adapter.create_session()
    db_adapter.create_log_entry(session, userinfo=userinfo)
    data = db_adapter.query_logs(session)
    for i in data:
        print(i.type)
        print(i.user_name)
        print(i.timestamp)
        print(i.url_accessed)
        print(i.cern_roles)
    session.close()

# Flask Blueprint object. Will be used to register routes.
sso_bp = Blueprint("sso_bp", __name__)

# SSO_CLIENT_ID and SSO_CLIENT_SECRET are retrieved from corresponding environment variables.
# Be sure to have them set correctly before executing the web server.
# SSO_CLIENT_ID = os.getenv('SSO_CLIENT_ID')
# SSO_CLIENT_SECRET = os.getenv('SSO_CLIENT_SECRET')

SSO_CLIENT_ID = "itk-demo-sw"
SSO_CLIENT_SECRET = "d4f068f0-d314-4522-be33-1c9bcab40a72"

# CERN_SSO endpoint to gather all necessary configuration
CERN_SSO = "https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration"


# Sets the OAuth provider with all necessary parameters
def create_oauth(app) -> OAuth:
    oauth_provider = OAuth(app)
    oauth_provider.register(
        name="cern",
        server_metadata_url=CERN_SSO,
        client_id=SSO_CLIENT_ID,
        client_secret=SSO_CLIENT_SECRET,
        client_kwargs={"scope": "openid email profile"},
    )
    return oauth_provider


oauth = create_oauth(current_app)

@sso_bp.route("/login")
def login():
    """SSO login endpoint. Redirects to the auth endpoint."""
    auth_uri = url_for("sso_bp.auth", _external=True, log_type="login")
    redirect_uri = request.args.get("redirect_uri")
    if not (redirect_uri.startswith("http://") or redirect_uri.startswith("https://")):
        redirect_uri = "http://" + redirect_uri
    auth_uri = f"{auth_uri}?redirect_uri={redirect_uri}"
    return oauth.cern.authorize_redirect(auth_uri)



# Route for seeing a data
@sso_bp.route("/user_data")
def get_user_data():
    """Endpoint to retrieve the authenticated user's data.

    Responses:
        200:
            A JSON with the user's data. If the user is not logged in, a JSON with
            a single field `"logged_in": False` is returned
    """
    # Every time user data is requested checks the token validity. If it's expired, tries to refresh it with the
    # refresh token. If an error is catched (due to refresh token expiration), removes the user to the local session,
    # forcing a new login to retrieve user data.
    if "token" in session:
        if session.get("token").get("expires_at") <= time.time():
            try:
                token = oauth.cern.fetch_access_token(
                    refresh_token=session.get("token").get("refresh_token"),
                    grant_type="refresh_token",
                )

            except OAuthError:
                session.clear()
            else:
                session["token"] = token

        user_data = session.get("userinfo")
        if user_data != None:
            return_data = {
                "username": user_data["cern_upn"],
                "name": user_data["name"],
                "cern_person_id": user_data["cern_person_id"],
                "email": user_data["email"],
                "cern_roles": user_data["cern_roles"],
                "cern_uid": user_data["cern_uid"],
                "cern_gid": user_data["cern_gid"],
                "logged_in": True,
            }
            return jsonify(return_data)

    return jsonify({"logged_in": False})


@sso_bp.route("/auth/<log_type>")
def auth(log_type):
    """SSO auth endpoint. Actually triggers the authentication procedure.
    After the auth is completed, redirects to the homepage
    """
    print(f"Log type: {log_type}")
    redirect_uri = request.args.get("redirect_uri")
    print(f"Redirect URL auth: {redirect_uri}")
    
    try:
        token = oauth.cern.authorize_access_token()
    except Exception as e:
        print(f"Exception occurred: {e}")

    userinfo = {"user_name": token["userinfo"]["sub"], "timestamp": token["userinfo"]["auth_time"], "url_accessed": redirect_uri, "cern_roles": str(token["userinfo"]["cern_roles"]), "type": log_type}
    
    log_user_info(userinfo)
    
    return redirect(f"{redirect_uri}?user={token["userinfo"]["sub"]}") 


@sso_bp.route("/logout")
def logout():
    """Logout endpoint.

    After logout redirects to homepage
    """
    # If the full parameter is false, the user is only deleted from local session, keeping the global SSO session valid.
    # If the full parameter is true, a full logout is performed, logging the user out of the entire SSO session
    # isFullLogout = request.args.get("full")
    # response = redirect("/")
    # if isFullLogout == "true":
    #     token = session["token"]

    #     # The SSO logout endpoint is retrieved from server metadata
    #     logout_uri = oauth.cern.load_server_metadata().get("end_session_endpoint")

    #     # The following is the necessary logout query, as instructed in the official CERN documentation
    #     # https://auth.docs.cern.ch/user-documentation/oidc/config/
    #     query = urlencode(
    #         {
    #             "client_id": SSO_CLIENT_ID,
    #             "id_token_hint": token["id_token"],
    #             "post_logout_redirect_uri": current_app.config["HOMEPAGE_URL"],
    #         }
    #     )
    #     response = redirect(logout_uri + "?" + query)

    # After logout, the session is cleared and the user is redirected to the homepage

    auth_uri = url_for("sso_bp.auth", _external=True, log_type="logout")

    redirect_uri = "https://itk-demo-sso.web.cern.ch/docs/"

    if not (redirect_uri.startswith("http://") or redirect_uri.startswith("https://")):
        redirect_uri = "http://" + redirect_uri
    auth_uri = f"{auth_uri}?redirect_uri={redirect_uri}"
    return oauth.cern.authorize_redirect(auth_uri)
    

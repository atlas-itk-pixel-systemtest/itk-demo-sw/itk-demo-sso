from itk_demo_sso.sqlalchemy_models import Base
from itk_demo_sso.tools import compressToBytes, decompressBytes
# from itk_demo_sso.base_payload import BasePayload
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
import json
from sqlalchemy_utils import UUIDType
from uuid import uuid4
from sqlalchemy.orm import declarative_base


class Payload(Base):
    """
    ORM-class that represents the payload-table
    """ 

    __tablename__ = "payload"

    data = Column(String)
    id = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    type = Column(String(255), nullable=False)

    # tags = relationship("Tag", secondary=tag_payload, back_populates="payloads")
    # objects = relationship("Object", secondary=object_payload, back_populates="payloads")

    def __init__(self, type, data=None, id=None):
        if id is not None:
            self.id = id
        if data is not None:
            self.data = data
        self.type = type

    # def to_dict(self, payload_data=True, decode=True, format=False):
    #     rep = {"id": self.id.hex, "type": self.type, "name": self.name, "meta": False}
    #     if payload_data:
    #         if self.data is not None:
    #             rep["data"] = self.get_data(decode=decode, format=format)
    #     return rep

    # def get_data(self, decode=True, format=False):
    #     if decode:
    #         if format:
    #             data = decompressBytes(self.data)
    #             try:
    #                 data = json.loads(data)
    #                 data = json.dumps(data, indent=4)
    #             except json.JSONDecodeError:
    #                 pass
    #             return data
    #         else:
    #             return decompressBytes(self.data)
    #     else:
    #         return self.data

    # def set_data(self, data, encode=True):
    #     if encode:
    #         self.data = compressToBytes(data)
    #     else:
    #         self.data = data

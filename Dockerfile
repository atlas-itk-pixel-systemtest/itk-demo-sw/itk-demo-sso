# Dockerfile for Flask application
FROM python:3.12-slim

WORKDIR /app

# Copy requirements.txt and install dependencies
COPY requirements.txt /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Copy the rest of the application code
COPY . .

# Expose port 5000
EXPOSE 5000

# Set environment variables
# ENV FLASK_APP=itk-demo-sso/app.py
# ENV FLASK_RUN_HOST=0.0.0.0

# Command to run the Flask application
#CMD ["flask", "run"]
CMD ["gunicorn", "wsgi:app", "--bind", "0.0.0.0:5000", "--workers=4", "--timeout", "600"]
ENV PYTHONUNBUFFERED=TRUE

#gunicorn wsgi:app --bind 0.0.0.0:5000 --workers=4 --timeout 600

from flask import Flask, redirect, url_for, session
from sso import login, get_user_data, logout

app = Flask(__name__)
app.secret_key = 'd4f068f0-d314-4522-be33-1c9bcab40a72'

from flask import Blueprint
from flask import url_for, session, jsonify, request, redirect, current_app, make_response
from authlib.integrations.flask_client import OAuth, OAuthError

import os, time, requests, jwt
from jwt import PyJWKClient
from urllib.parse import urlencode

# Flask Blueprint object. Will be used to register routes.
sso_bp = Blueprint('sso_bp', __name__)

# SSO_CLIENT_ID and SSO_CLIENT_SECRET are retrieved from corresponding environment variables.
# Be sure to have them set correctly before executing the web server.
SSO_CLIENT_ID = os.getenv('SSO_CLIENT_ID')
SSO_CLIENT_SECRET = os.getenv('SSO_CLIENT_SECRET')

# CERN_SSO endpoint to gather all necessary configuration
CERN_SSO = 'https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration'

# Sets the OAuth provider with all necessary parameters
oauth = OAuth(current_app)
oauth.register(
    name='cern',
    server_metadata_url = CERN_SSO,
    client_id = SSO_CLIENT_ID,
    client_secret = SSO_CLIENT_SECRET,
    client_kwargs = {
        'scope': 'openid email profile'
    }
)


@sso_bp.route('/login')
def login():
    """SSO login endpoint. Redirects to the auth endpoint."""
    redirect_uri = url_for('sso_bp.auth', _external=True)
    return oauth.cern.authorize_redirect(redirect_uri)


# Route for seeing a data
@sso_bp.route('/user-data')
def get_user_data():
    """Endpoint to retrieve the authenticated user's data.
    
    Responses:
        200: 
            A JSON with the user's data. If the user is not logged in, a JSON wiht 
            a single field `"logged_in": False` is returned
    """
    # Every time user data is requested checks the token validity. If it's expired, tries to refresh it with the 
    # refresh token. If an error is catched (due to refresh token expiration), removes the user to the local session, 
    # forcing a new login to retrieve user data.
    if 'token' in session:
        if session.get('token').get('expires_at') <= time.time():
            try:
                token = oauth.cern.fetch_access_token(refresh_token=session.get('token').get('refresh_token'), grant_type='refresh_token')
            except OAuthError:
                session.clear()
            else:
                session['token'] = token

        user_data = session.get('userinfo')
        if user_data != None:
            return_data = {
                "username": user_data['cern_upn'],
                "name": user_data['name'],
                "cern_person_id": user_data['cern_person_id'],
                "email": user_data['email'],
                "cern_roles": user_data['cern_roles'],
                "cern_uid": user_data['cern_uid'],
                "cern_gid": user_data['cern_gid'],
                "logged_in": True
            }
            return jsonify(return_data)
        
    return jsonify({"logged_in": False})


@sso_bp.route('/auth')
def auth():
    """SSO auth endpoint. Actually triggers the authentication procedure.
    After the auth is completed, redirects to the homepage
    """

    # Tries to get a token
    token = oauth.cern.authorize_access_token()

    # Gets the user information from the relative endpoint (necessary to trigger this endpoint
    # since not all the needed data is in the id_token)
    userinfo = oauth.cern.userinfo()

    # Decodes the id_token with keys retrieved from the jwks_uri endpoint
    jwks_uri = oauth.cern.server_metadata.get('jwks_uri')
    jwks_client = PyJWKClient(jwks_uri)
    signing_key = jwks_client.get_signing_key_from_jwt(token.get('id_token'))
    data = jwt.decode(
        token.get('id_token'),
        signing_key.key,
        algorithms=["RS256"],
        audience=SSO_CLIENT_ID,
    )

    # When the jwt version in tdaq relase gets bumped >= 2.8.0 (latest release as of 05 Sept 2023)
    # the following logic can be added to test token hash (the base64 library should be imported)
    #
    # # get the pyjwt algorithm object
    # alg_obj = jwt.get_algorithm_by_name("RS256")
    # 
    # digest = alg_obj.compute_hash_digest(token.get('id_token'))
    # at_hash = base64.urlsafe_b64encode(digest[: (len(digest) // 2)]).rstrip("=")
    # assert at_hash == data

    # Sets user roles from decoded id_token
    userinfo['cern_roles'] = data.get('cern_roles')

    # Saves data to session
    session['userinfo'] = userinfo
    session['token'] = token
    
    # Redirects to homepage
    return redirect('/')


@sso_bp.route('/logout')
def logout():
    """Logout endpoint.
    
    After logout redirects to homepage
    """
    # If the full parameter is false, the user is only deleted from local session, keeping the global SSO session valid.
    # If the full parameter is true, a full logout is performed, logging the user out of the entire SSO session
    isFullLogout = request.args.get('full')
    response = redirect('/')
    if (isFullLogout == "true") :
        token = session['token']

        # The SSO logout endpoint is retrieved from server metadata
        logout_uri = oauth.cern.load_server_metadata().get('end_session_endpoint')

        # The following is the necessary logout query, as instructed in the official CERN documentation
        # https://auth.docs.cern.ch/user-documentation/oidc/config/
        query = urlencode({'client_id': SSO_CLIENT_ID, 'id_token_hint': token['id_token'], 'post_logout_redirect_uri': current_app.config['HOMEPAGE_URL']})
        response = redirect(logout_uri + '?' + query)
    
    # After logout, the session is cleared and the user is redirected to the homepage
    session.clear()
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
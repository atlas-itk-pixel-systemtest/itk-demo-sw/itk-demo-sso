"""Routes for parent flask app"""

from flask import current_app as app
from flask import jsonify, render_template
# from helpers.decorators import login_required


# @app.route("/dbs-config")
# @login_required
# def dbs_config():
#     """Endpoint for database configuration.
    
#     Returns databases configuration according to the app configuration
#     """
#     return jsonify({"oracle": app.config["IS_ORACLE"], "sqlite": app.config["IS_SQLITE"]})


# @app.route('/site-settings')
# def site_settings():
#     """Endpoint for site settings.
    
#     Returns a JSON describing the file settings (i.e. the blueprints being used) according to the app configuration
#     """
#     return jsonify({"sso": app.config["IS_SSO"], 
#                     "database": (app.config['IS_ORACLE'] or app.config['IS_SQLITE']),
#                     "ldap": app.config['IS_LDAP'],
#                     "process_runner": app.config['IS_PROCESS_RUNNER']})


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    """Catch-all Route
    """
    return render_template('index.html')
